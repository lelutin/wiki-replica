IMPORTANT NOTE: most Tor servers do *not* currently use nftables, as
we still use the Ferm firewall wrapper, which only uses
iptables. Still, we sometimes end up on machines that might have
nftables and those instructions will be useful for that brave new
future. See [tpo/tpa/team#40554](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40554) for a followup on that migration.

[[_TOC_]]

# Listing rules

    nft -a list ruleset

The `-a` flag shows the handles which is useful to delete a specific
rule.

# Checking and applying a ruleset

This checks the ruleset of Puppet rule files as created by the
puppet/nftables modules before applying it:

    nft -c -I /etc/nftables/puppet -f /etc/nftables/puppet.nft

This is done by Puppet before actually applying the ruleset, which is
done with:

    nft -I /etc/nftables/puppet -f /etc/nftables/puppet.nft

The `-I` parameter stands for `--includepath` and tells `nft` to look
for rules in that directory.

# Inserting a rule to bypass a restriction

Say you have the chain `INPUT` in the table `filter` which looks like
this:

    table inet filter {
    	chain INPUT {
    		type filter hook input priority filter; policy drop;
    		iifname "lo" accept
    		ct state established,related accept
    		ct state invalid drop
    		tcp dport 22 accept
    		reject
    	}
    }

.. and you want to temporarily give access to the web server on
port 443. You would do a command like:

    nft insert rule inet filter INPUT 'tcp dport 443 accept'

Or if you need to allow a specific IP, you could do:

    nft insert rule inet filter INPUT 'ip saddr 192.0.2.0/24 accept'

# Blocking a host

Similarly, assuming you have the same `INPUT` chain in the `filter`
table, you could do this to block a host from accessing the server:

    nft insert rule inet filter INPUT 'ip saddr 192.0.2.0/24 reject'

That will generate an ICMP response. If this is a DOS condition, you
might rather avoid that and simply drop the packet with:

    nft insert rule inet filter INPUT 'ip saddr 192.0.2.0/24 reject'

# Deleting a rule

If you added a rule by hand in the above and now want to delete it,
you first need to find the handle (with the `-a` flag to `nft list
ruleset`) and then delete the rule:

    nft delete rule inet filter INPUT handle 39

Be VERY CAREFUL with this step as using the wrong handle might lock
you out of the server.

# Other documentation

 * [nftables wiki](https://wiki.nftables.org/wiki-nftables/), and especially:
   * [quick reference](https://wiki.nftables.org/wiki-nftables/index.php/Quick_reference-nftables_in_10_minutes)
   * [iptables to nftables translation](https://wiki.nftables.org/wiki-nftables/index.php/Moving_from_iptables_to_nftables) 
 * [RHEL nftables reference](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_networking/getting-started-with-nftables_configuring-and-managing-networking)
 * [Debian wiki](https://wiki.debian.org/nftables)
 * [Arch wiki](https://wiki.archlinux.org/title/nftables)
