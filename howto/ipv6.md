# Creating a tunnel with HE.net

https://tunnelbroker.net/

https://tunnelbroker.net/new_tunnel.php

enter the IP address of your endpoint (your current IP address is
shown and can be copy-pasted if you're already on site)

pick a location and hit "Create tunnel"

then you can add the description (optional)

you can copy the configuration which, for Debian, looks like:

```
auto he-ipv6
iface he-ipv6 inet6 v4tunnel
        address 2001:470:1c:81::2
        netmask 64
        endpoint 216.66.38.58
        local 216.137.119.51
        ttl 255
        gateway 2001:470:1c:81::1
```

TODO: replace the above with sample IP addresses

Note that, in the above configuration, you do not have access to the
entire /64 the `gateway` and `address` live under. They use a /64 for
a point to point link because of [RFC2627](https://www.rfc-editor.org/rfc/rfc3627.html). The network you will
announce locally will be different, under the "Routed IPv6 Prefixes"
section. For example, in my case it is `2001:470:1d:81::/64` and I
have the option to add a `/48` if I need more networks.

If you have a dynamic IP address, you will need to setup a [dynamic
update](https://forums.he.net/index.php?topic=1994.0) of your IP address, so that your endpoint gets update
correctly on their end. Information about those parameters is in the
"Advanced" tab of your tunnel configuration. There you can also
unblock IRC and SMTP access.
