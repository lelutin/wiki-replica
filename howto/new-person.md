---
title: How to get a new Tor System Administrator (with web developer duties) on board
---

Note that this documentation needs work, as it overlaps with normal
user management procedures, see [issue 40129](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40129). Also note that the
operations people have their own [onboarding project](https://gitlab.torproject.org/tpo/operations/employees/onboarding) to track this
work as well.

It might also be better on the frontpage of the wiki, or a more
general introduction page.

# Glossary

 * TSA: Tor System Administrators
 * TPA: Tor Project Admins, synonymous with TSA, preferably used to
   disambiguate with [the other TSAs](https://en.wikipedia.org/wiki/TSA)
 * TPI: Tor Project Inc. the company that employs Tor staff
 * TPO: `torproject.org`, machines officially managed by TSA, often
   shortened as `.tpo`, for example. `www.tpo`
 * `torproject.net`, machines in DNS but not officially managed by TSA
 * a sysadmin can also be a service admin, and both can be paid work

# Orienteering

 * [sysadmin wiki](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/)
 * [service list](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service)
 * [machines list](https://db.torproject.org/machines.cgi)
 * key machines:
   * [jump host](doc/ssh-jump-host): `ssh-dal.torproject.org` and
     `ssh-fsn`, for North America and Europe
   * home pages and shell server: `people.torproject.org` AKA `perdulce`
   * [IRC](howto/irc) bouncer: `chives`
   * [Puppet](howto/puppet): `pauli`
   * [LDAP](howto/ldap): `db.torproject.org` AKA `alberti`
   * Main mail server: `eugeni`
   * Master [Ganeti](howto/ganeti) nodes: `fsn-node-01`, `dal-node-01`
 * key services:
   * [GitLab](howto/gitlab): <https://gitlab.torproject.org/> - issue tracking,
     project management, and git repository hosting
   * [git repositories list](https://gitlab.torproject.org/tpo/tpa/repos), clone this first
   * [web sites and team](https://gitlab.torproject.org/tpo/web/team)
   * [Grafana](howto/grafana): <https://grafana.torproject.org> - monitoring
     dashboard, password in [password manager](service/password-manager)
   * [Nagios](howto/nagios):
     <https://nagios.torproject.org/cgi-bin/icinga/status.cgi?allunhandledproblems>
     legacy alert dashboard
   * see also the full [service list](service)
 * how the team works:
   * [meetings][]:
     * TPA has weekly checkins, monthly [roadmap](roadmap), and yearly
       in-person meetings
     * 1:1s: monthly
     * TPI has online All hands every week on Wednesday and yearly in-person
   * IRC / BBB / Signal / right to disconnect
   * [support][]: "star of the week" shift rotation
   * issue dashboards: [TPA](https://gitlab.torproject.org/groups/tpo/tpa/-/boards), [web](https://gitlab.torproject.org/groups/tpo/web/-/boards)
   * [roadmap][], [policies][]
   * calendars:
     * TPA team: tracking meetings and sometimes rotation
     * AFK tracker: to update when you take a vacation, leave, or
       holiday
     * TPI holidays: US public holidays
   * see also employee handbook from HR
 * mailing lists:
   * <tor-project@lists.torproject.org> - Open list where anyone is
     welcome to watch but posting is moderated. Please favor using
     this when you can.
   * <tor-internal@lists.torproject.org> - If something truly can't
     include the wider community then this is the spot.
   * <tor-team@lists.torproject.org> - Exact same as tor-internal@
     except that the list will accept email from non-members. If you
     need a cc when emailing a non-tor person then this is the place.
   * <tor-employees@lists.torproject.org> - TPI staff mailing list
   * <tor-meeting@lists.torproject.org> - for public meetings
   * <torproject-admin@torproject.org> - TPA-specific "mailing list"
     (not a mailing list but an alias)
   * <tpa-team@lists.torproject.org> - TPA team mailing list
   * see the [list of mailing lists](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/lists#list-of-mailing-lists) for a more exhaustive list

 * IRC channels:
   * `#tor-project` - general Tor project channel
   * `#tor-admin` - channel for TPA specific stuff
   * `#tor-www` - Tor websites development channel
   * `#tor-internal` - channel for private discussions, need secret
     password and being added to the `@tor-tpomember` with `GroupServ`,
     part of the `tor-internal@lists.tpo` welcome email)
   * `#tor-bots` - where a lot of bots live
   * `#tor-nagios` ... except the Nagios bot, which lives here
   * `#tor-meeting` - where some meetings are held
   * `#tor-meeting2` - fallback for the above

# Important documentation

 1. [Getting to know LDAP](howto/ldap#getting-to-know-ldap)
 2. [SSH jump host configuration](doc/ssh-jump-host)
 3. [How to edit this wiki](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/documentation#editing-the-wiki-through-git), make sure you have a local copy of the
    documentation!
 4. [Puppet primer: adding yourself to the allow list](howto/puppet#adding-an-ip-address-to-the-global-allow-list)
 5. [New machine creation](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/new-machine)
 6. [Updating status.tpo](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/status#creating-new-issues)
 7. [Tor Websites](https://gitlab.torproject.org/tpo/web/team)
 8. [Roadmap](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap) and [Policies][]

# More advanced documentation

 1. [Account creation procedures](howto/create-a-new-user)
 2. [Password management](service/password-manager)
 3. [Adding](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/static-component#adding-a-new-component) and [removing](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/static-component#removing-a-component) websites in the [static mirror
    system](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/static-component)
 4. [Editing DNS](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/dns#editing-a-zone)
 5. [TLS certificate operations](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/tls#how-to-get-an-x509-certificate-for-a-domain-with-lets-encrypt)
 6. [Puppet code linting](howto/puppet#validating-puppet-code) and the entire [Puppet operations manual](howto/puppet)
 7. [Backup restore procedures](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/backup)
 8. [Documentation design](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/documentation#design)
 9. [Ganeti operations manual](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/ganeti)

The full documentation is available in the wiki and particularly from
the [service list](service).

# Accounts to create

This section is specifically targeted at *existing* sysadmins, which
should follow this checklist to create the necessary accounts on all
core services. More services might be required if the new person is
part of other service teams, see the [service list](service) for the
exhaustive list.

The first few steps are part of the TPI onboarding process and might
already have been performed.

Here's a checklist that should be copy-pasted in a ticket.

## Basic TPA access

 1. [ ] schedule onboarding meeting, ideally in the future 1:1 time
        slot
 1. [ ] send welcome email [see new-person](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/new-person)
 1. [ ] [New LDAP account](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/create-a-new-user), instruct user on [email procedures](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/email)
 1. [ ] mailing lists (`tor-internal@` and others, see list above)
 1. [ ] [GitLab account](/users/sign_in), if not already done, can be done by the
        user through [anonticket](https://anonticket.torproject.org/user/gitlab-account/create/) otherwise create the account
        through the admin interface and let the user perform password reset
 1. [ ] [about/people](https://torproject.org/about/people) web page (new person should issue a MR
        against the [source code](https://gitlab.torproject.org/tpo/web/tpo/-/tree/master/content/about/people) and then get approved)
 1. [ ] GitLab [`tpo/tpa`](/tpo/tpa) group membership, "Maintainer" or
        "Owner" level (this also grants access to `tpo/team` and
        `tpo/web`, among other things)
 1. [ ] [Nagios](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/nagios) access, contact should be created in git
    repository `nagiosadm@nagios.torproject.org:/home/nagiosadm/tor-nagios`,
    password in `/etc/icinga/htpasswd.users` directly on the server
 1. [ ] [Nextcloud](https://nc.torproject.net) account with groups TPI and TPA, user can
        perform password reset on their own
 1. [ ] `torproject-admin@` and `torproject-admin-vcs@` aliases
 1. [ ] make sure HR/ops have a ticket to followup with other accesses
        (time tracker, payroll, etc)

Many of those are granted as part of the routine "core tor membership" admission process.

## Full TPA access

Other accounts required for full TPA access, those require the person
to be vetted by a member of the community as they give access to
*everything*:

 1. [ ] [LDAP admin access](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/ldap#addingremoving-an-admin)
 2. [ ] [Puppet](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/puppet#adding-removing-a-global-admin) git repository access
 3. [ ] [TPA password manager access](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/password-manager#on-boarding-new-staff) (`tor-passwords.git` on the Puppet
        server)
 4. [ ] Safespring cloud access (e.g. `Message-ID: <87bk4fru9n.fsf@angela.anarc.at>`)

## Other services

Extra services we are not directly responsible for, but that TPA staff
may administer at some point. Those are given as needed, depending on
which service the new person will be "service admin" for:

 1. [ ] [BBB](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/conference) access
 2. [ ] GitLab `-admin` account
 3. [ ] [Nextcloud](https://nc.torproject.net) admin account
 4. [ ] [RT](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/rt#new-rt-admin)
 5. [ ] [torproject github account](https://github.com/torproject)

Those are purely optional. See the [service list](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service) for more ideas.

# Welcome email

This email should be edited and sent to the hired candidates when they
are confirmed.

---

Hi X!

First of all, congratulations and welcome to TPI (Tor Project, Inc.)
and the TPA (Admin) team. Exciting times!

We'd like you to join us on your first orientation meeting on TODO
Month day, TODO:00 UTC (TODO:00 your local time), in my [home
room][]. Also note that we have our weekly check-in on Monday at
18:00UTC as well.

 [home room]: TODO

Make sure you can attend the meeting and pen it down in your
calendar. If you cannot make it for some reason, please do let us know
as soon as possible so we can reschedule.

Here is the agenda for the meeting:

TODO: copy paste from the [OnBoardingAgendaTemplate](https://gitlab.torproject.org/tpo/team/-/wikis/OnBoardingAgendaTemplate), and append:

 2. Stakeholders for your work:
    - TPA
    - web team
    - consultants
    - the rest of Tor...
 3. How the TPA team works:
    - [meetings][] (weekly and yearly face-to-face)
    - IRC / BBB / Signal / right to disconnect
    - [support][]: "star of the week" shift rotation
    - issues: [TPA board][], [web board][]
    - [wiki][]: [service list][], [policies][], [roadmap][]
 4. TPA systems crash course through the [new-person][] wiki page

 [meetings]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/meeting/
 [support]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/support
 [TPA board]: https://gitlab.torproject.org/groups/tpo/tpa/-/boards
 [web board]: https://gitlab.torproject.org/groups/tpo/web/-/boards
 [wiki]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/home
 [service list]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service
 [policies]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/policy
 [roadmap]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap

You will shortly receive the following credentials, in an OpenPGP
encrypted email:

 - an LDAP account
 - a Nextcloud account
 - a GitLab account (if you already have one, let us know!)

You should do the following with these accesses:

 1. hook your favorite calendar application with your Nextcloud account
 2. configure an SSH key in LDAP
 3. login to `people.torproject.org` (aka `perdulce`) and
    download the known hosts, see the [jump host documentation][] on
    how to partially automate this
 4. if you need an IRC bouncer, login to `chives.torproject.org` and
    setup a screen/tmux session, or ask `@pastly` on IRC to get access
    to the ZNC bouncer
 5. provide a merge request on [about/people][] to add your bio and
    picture

 [about/people]: https://gitlab.torproject.org/tpo/web/tpo/-/tree/master/content/about/people
 [jump host documentation]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/doc/ssh-jump-host
 [Nagios]: https://nagios.torproject.org/

So you also have a lot of reading to do already! The [new-person][] page
is a good reference to get started.

 [new-person]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/new-person

But take it slowly! It can be overwhelming to join a new organisation
and it will take you some time to get acquainted with everything. Don't
hesitate to ask if you have any questions!

See you soon, and welcome aboard!
