[[_TOC_]]

# Service documentation

This documentation covers all services hosted at TPO.

Every service hosted at TPO should have a documentation page, either
in this wiki, or elsewhere (but linked here). Services should ideally
follow this [template](howto/template) to ensure proper documentation.

## Internal services

Those are services managed by TPA directly.

| Service              | Purpose                           | URL                                 | Maintainers         | Documented | Auth                                                                |
|----------------------|-----------------------------------|-------------------------------------|---------------------|------------|---------------------------------------------------------------------|
| [backup][]           | Backups                           | N/A                                 | TPA                 | 75%        | N/A                                                                 |
| [btcpayserver][]     | BTCpayserver                      | <https://btcpay.torproject.org/>    | TPA sue             | 90%        | yes                                                                 |
| [CDN][]              | content-distribution network      | varies                              | TPA                 | 80%        | yes                                                                 |
| [CRM][]              | Donation management               | `https://crm.torproject.org`        | symbiotic TPA       | 5%         | yes                                                                 |
| [dangerzone][]       | Sanitize untrusted documents      | N/A                                 | TPA                 | 100%       | LDAP Nextcloud                                                      |
| [dns][]              | domain name service               | N/A                                 | TPA                 | 10%        | N/A                                                                 |
| [documentation][]    | documentation (this wiki)         | <https://help.torproject.org/>      | TPA                 | 10%        | see GitLab                                                          |
| [donate-review][]    | donate-neo review apps            | `*.donate-review.torproject.net`    | TPA lavamind        | 90%        | N/A                                                                 |
| [drbd][]             | disk redundancy                   | N/A                                 | TPA                 | 10%        | N/A                                                                 |
| [email][]            | @torproject.org emails services   | N/A                                 | TPA                 | 0%         | LDAP Puppet                                                         |
| [forum][]            | Tor Project community forums      | <https://forum.torproject.net>      | TPA hiro gus duncan | 50%        | yes                                                                 |
| [ganeti][]           | virtual machine hosting           | N/A                                 | TPA                 | 90%        | no                                                                  |
| [gitlab][]           | Issues, wikis, source code        | <https://gitlab.torproject.org/>    | TPA ahf gaba        | 90%        | yes                                                                 |
| [grafana][]          | metrics dashboard                 | <https://grafana.torproject.org>    | TPA anarcat         | 10%        | [Puppet](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40124) |
| [ipsec][]            | VPN                               | N/A                                 | TPA                 | 30%        | Puppet                                                              |
| [ldap][]             | host and user directory           | <https://db.torproject.org>         | TPA                 | 90%        | yes                                                                 |
| [lists][]            | Mailing lists                     | <https://lists.torproject.org>      | TPA arma atagar qbi | 20%        | yes                                                                 |
| [logging][]          | centralized logging               | N/A                                 | TPA                 | 10%        | no                                                                  |
| [nagios][]           | alerting                          | <https://nagios.torproject.org>     | TPA                 | 5%         | Puppet and on-server                                                |
| [object-storage][]   | S3-like object storage            | N/A                                 | TPA                 | 100%       | access keys                                                         |
| [openstack][]        | virtual machine hosting           | N/A                                 | TPA                 | 30%        | yes                                                                 |
| [password-manager][] | password management               | N/A                                 | TPA                 | 30%        | Git                                                                 |
| [postgresql][]       | database service                  | N/A                                 | TPA                 | 80%        | no                                                                  |
| [prometheus][]       | metrics collection and monitoring | <https://prometheus.torproject.org> | TPA                 | 90%        | no                                                                  |
| [puppet][]           | configuration management          | `puppet.torproject.org`             | TPA                 | 100%       | yes                                                                 |
| [static-component][] | static site mirroring             | N/A                                 | TPA                 | 90%        | LDAP                                                                |
| [static-shim][]      | static site / GitLab shim         | N/A                                 | TPA                 |            | no                                                                  |
| [status][]           | status dashboard                  | N/A                                 | TPA anarcat         | 100%       | no                                                                  |
| [survey][]           | survey application                | <https://survey.torproject.org/>    | TPA gaba lavamind   | 1%         | yes                                                                 |
| [survey][]           | limesurvey instance               | N/A                                 | TPA lavamind        | 50%        | yes                                                                 |
| [svn][]              | Document storage                  | <https://svn.torproject.org/>       | unmaintained        | 10%        | yes                                                                 |
| [tls][]              | X509 certificate management       | N/A                                 | TPA                 | 50%        | no                                                                  |
| [website][]          | main website                      | <https://www.torproject.org>        | TPA gus             | ?          | LDAP                                                                |
| [wkd][]              | OpenPGP certificates distribution | N/A                                 | TPA                 | 10%        | yes                                                                 |

The `Auth` column documents whether the service should be audited for
access when a user is retired. If set to "LDAP", it means it should be
revoked to a LDAP group membership change. In the case of "Puppet",
it's because the user might have access through that as well.

It is estimated that, on average, 42% of the documentation above is
complete. This does not include undocumented services, below.

[backup]: howto/backup
[dns]: howto/dns
[documentation]: service/documentation
[donate-review]: service/donate-review
[drbd]: howto/drbd
[email]: service/email
[ganeti]: howto/ganeti
[grafana]: howto/grafana
[ipsec]: howto/ipsec
[kvm]: howto/kvm
[ldap]: howto/ldap
[logging]: howto/logging
[nagios]: howto/nagios
[object-storage]: service/object-storage
[openstack]: howto/openstack
[postgresql]: howto/postgresql
[prometheus]: howto/prometheus
[puppet]: howto/puppet
[static-component]: howto/static-component
[static-shim]: service/static-shim
[status]: service/status
[survey]: service/survey
[tls]: howto/tls
[wkd]: howto/Web-Key-Directory-[wkd]---How-keys-are-managed-in-Tor
[password-manager]: service/password-manager

## Non-TPA services

The following table lists services run on torproject
infrastructure. Corresponding onion services are listed on
<https://onion.torproject.org/>.

Service admins are part of tor project sys admins team. For a rough
description of what sys admin and services admin do, please have a
look [here](policy/tpa-rfc-2-support#service-admins).

The Service Admins maintain the following list of Tor Services.

| Service                | Purpose                                               | URL                                     | Maintainers       | Documented | Auth                          |
|------------------------|-------------------------------------------------------|-----------------------------------------|-------------------|------------|-------------------------------|
| [anon_ticket][]        | Anonymous ticket lobby for GitLab                     | <https://anonticket.torproject.org/>    | ahf juga          | 10%        | no                            |
| [apps team builders][] | build Tor Browser and related                         | N/A                                     | richard           | 10%        | LDAP                          |
| [BBB][]                | Video and audio conference system                     | <https://tor.meet.coop>                 | gaba gus          | -          | yes (see [policy][])          |
| [blog][]               | Weblog site                                           | <https://blog.torproject.org/>          | lavamind gus      | 90%        | GitLab                        |
| [bridgedb][]           | web app and email responder to learn bridge addresses | <https://bridges.torproject.org/>       | cohosh meskio     | 20%        | no                            |
| [bridgestrap][]        | service to tests bridges                              | `https://bridges.torproject.org/status` | cohosh meskio     | 20%        | no                            |
| [check][]              | Web app to check if we're using tor                   | <https://check.torproject.org>          | arlolra           | 90%        | LDAP                          |
| [collector][]          | Collects Tor network data and makes it available      | collector{1,2}.torproject.org           | irl               | ?          | ?                             |
| [debian archive][]     | Debian package repository                             | <https://deb.torproject.org>            | weasel            | 20%        | LDAP                          |
| [gettor][]             | email responder handing out packages                  | <https://gettor.torproject.org>         | cohosh meskio     | 10%        | no                            |
| [irc][]                | IRC bouncer and network                               | `ircbouncer.torproject.org`             | pastly            | 90%        | yes (ZNC and @groups on OFTC) |
| [metrics][]            | Network descriptor aggregator and visualizer          | <https://metrics.torproject.org>        | irl               | ?          | ?                             |
| [moat][]               | Distributes bridges over domain fronting              |                                         | cohosh            | ?          | no                            |
| [nextcloud][]          | NextCloud                                             | <https://nc.torproject.net/>            | anarcat gaba      | 30%        | yes                           |
| [newsletter][]         | Tor Newsletter                                        | <https://newsletter.torproject.org>     | gus               | ?          | LDAP                          |
| [onionperf][]          | Tor network performance measurements                  | ?                                       | hiro acute ahf    | ?          | ?                             |
| [ooni][]               | Open Observatory of Network Interference              | <https://ooni.torproject.org>           | hellais           | ?          | no                            |
| [schleuder][]          | Encrypted mailing lists                               |                                         | anarcat dgoulet   | 30%        | yes                           |
| [rdsys][]              | Distribution system for circumvention proxies         | N/A                                     | cohosh meskio     | 20%        | no                            |
| [rt][]                 | Email support                                         | <https://rt.torproject.org/>            | gus gaba lavamind | 50%        | yes                           |
| [snowflake][]          | Pluggable Transport using WebRTC                      | <https://snowflake.torproject.org/>     | cohosh meskio     | 20%        | no                            |
| [styleguide][]         | Style Guide                                           | <https://styleguide.torproject.org>     | antonela          | 1%         | LDAP                          |
| [support portal][]     | Support portal                                        | <https://support.torproject.org>        | gus               | 30%        | LDAP                          |
| [vault][]              | Secrets storage                                       | <https://vault.torproject.org/>         | micah             | 10%        | yes                           |
| [weather][]            | Relay health monitoring                               | <https://weather.torproject.org/>       | sarthikg gk       | ?          | yes                           |

The `Auth` column documents whether the service should be audited for
access when a user is retired. If set to "LDAP", it means it should be
revoked to a LDAP group membership change. In the case of "Puppet",
it's because the user might have access through that as well.

Every service listed here must have some documentation, ideally
following the [documentation template](howto/template). As a courtesy,
TPA allows teams to maintain their documentation in a single page
here. If the documentation needs to expand beyond that, it should be
moved to its own wiki, but still linked here.

There are more (undocumented) services, listed below. Of the 20
services listed above, 6 have an unknown state because the
documentation is external (marked with `?`). Of the remaining 14
services, it is estimated that 38% of the documentation is complete.

[anon_ticket]: service/anon_ticket
[BBB]: howto/conference
[blog]: service/blog
[bridgedb]: https://gitlab.torproject.org/tpo/anti-censorship/team/-/wikis/Survival-Guides/BridgeDB-Survival-Guide
[bridgestrap]: https://gitlab.torproject.org/tpo/anti-censorship/team/-/wikis/Survival-Guides/Bridgestrap-Survival-Guide
[btcpayserver]: service/BTCpayserver
[check]: https://gitlab.torproject.org/tpo/network-health/team/-/wikis/metrics/exit-list-check/Check
[CDN]: service/cdn
[crm]: service/crm
[collector]: https://gitlab.torproject.org/tpo/network-health/team/-/wikis/metrics/collector/home
[dangerzone]: service/dangerzone
[debian archive]: service/debian-archive
[forum]: service/forum
[gettor]: https://gitlab.torproject.org/tpo/anti-censorship/gettor-project/gettor/-/wikis/Gettor-Service-Operator-HowTo
[git]: howto/git
[gitlab]: howto/gitlab
[irc]: howto/irc
[lists]: service/lists
[metrics]: https://gitlab.torproject.org/tpo/network-health/team/-/wikis/metrics/website/home
[moat]: https://gitlab.torproject.org/tpo/anti-censorship/team/-/wikis/Survival-Guides/Moat-Survival-Guide
[newsletter]: https://gitlab.torproject.org/tpo/web/newsletter/-/blob/master/README.md
[nextcloud]: service/nextcloud
[onionperf]: https://gitlab.torproject.org/tpo/network-health/team/-/wikis/metrics/OnionPerf/onionperf-ops
[ooni]: https://ooni.org/
[policy]: howto/conference#account-policy
[rdsys]: https://gitlab.torproject.org/tpo/anti-censorship/team/-/wikis/Survival-Guides/Rdsys-Survival-Guide
[rt]: howto/rt
[snowflake]: https://gitlab.torproject.org/tpo/anti-censorship/team/-/wikis/Survival-Guides/Snowflake-Broker-Survival-Guide
[schleuder]: service/schleuder
[submission]: howto/submission
[styleguide]: service/styleguide
[support portal]: service/support
[svn]: howto/svn
[vault]: service/vault
[weather]: service/tor-weather
[website]: https://gitlab.torproject.org/tpo/web/tpo/
[apps team builders]: https://gitlab.torproject.org/tpo/applications/tor-browser/-/wikis/Tor-Browser-Build-Machines

## Undocumented service list

WARNING: this is an import of an old Trac wiki page, and no
documentation was found for those services. Ideally, each one of those
services should have a documentation page, either here or in their
team's wiki.

| Service          | Purpose                                                                       | URL                                        | Maintainers  | Auth  |
|------------------|-------------------------------------------------------------------------------|--------------------------------------------|--------------|-------|
| archive          | package archive                                                               | <https://archive.torproject.org/>          | [boklm][]    | LDAP? |
| community        | Community Portal                                                              | <https://community.torproject.org>         | Gus          | no    |
| consensus-health | periodically checks the Tor network for consensus conflicts and other hiccups | <https://consensus-health.torproject.org>  | tom          | no?   |
| dist             | packages                                                                      | <https://dist.torproject.org>              | arma         | LDAP? |
| DocTor           | DirAuth health checks for the [tor-consensus-health@ list][]                  | <https://gitweb.torproject.org/doctor.git> | GeKo         | no    |
| exonerator       | website that tells you whether a given IP address was a Tor relay             | <https://exonerator.torproject.org/>       | irl          | ?     |
| extra            | static web stuff referenced from the blog (create trac ticket for access)     | <https://extra.torproject.org>             | tpa          | LDAP? |
| media            | ?                                                                             | <https://media.torproject.org>             |              | LDAP  |
| metricsbot       | Tor Network Status Bot (IRC, Twitter, Mastodon)                               |                                            | irl          | ?     |
| onion            | list of onion services run by the Tor project                                 | <https://onion.torproject.org>             | weasel       | no    |
| onionoo          | web-based protocol to learn about currently running Tor relays and bridges    |                                            | irl          | ?     |
| people           | content provided by Tor people                                                | <https://people.torproject.org>            | tpa          | LDAP  |
| research         | website with stuff for researchers including tech reports                     | <https://research.torproject.org>          | arma         | LDAP  |
| rpm archive      | RPM package repository                                                        | <https://rpm.torproject.org>               | kushal       | LDAP  |
| stem             | stem project website and tutorial                                             | <https://stem.torproject.org/>             | atagar       | LDAP? |
| tb-manual        | Tor Browser User Manual                                                       | <https://tb-manual.torproject.org/>        | gus          | LDAP? |
| testnet          | Test network services                                                         | ?                                          | dgoulet      | ?     |

[boklm]: https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/18629
[tor-consensus-health@ list]: https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-consensus-health

The `Auth` column documents whether the service should be audited for
access when a user is retired. If set to "LDAP", it means it should be
revoked to a LDAP group membership change. In the case of "Puppet",
it's because the user might have access through that as well.

## Research

Those services have not been implemented yet but are at the research
phase.

| Service | Purpose | URL | Maintainers |
|---------|---------|-----|-------------|
| N/A     |         |     |             |

## Retired

Those services have been retired.

| Service       | Purpose                              | URL                                    | Maintainers           | Fate                                      |
|---------------|--------------------------------------|----------------------------------------|-----------------------|-------------------------------------------|
| Atlas         | Tor relay discover                   | `https://atlas.torproject.org`         | irl                   | Replaced by metrics.tpo                   |
| [cache][]     | Web caching/accelerator/CDN          | N/A                                    | TPA                   | Cached site (blog) migrated to TPO infra  |
| Compass       | AS/country network diversity         | `https://compass.torproject.org`       | karsten               | ?                                         |
| fpcentral.tbb | browser fingerprint analysi          | `https://fpcentral.tbb.torproject.org` | boklm                 | Abandoned for [better][] [alternatives][] |
| [git][]       | Source control system                | <https://git.torproject.org>           | ahf, nickm, Sebastian | Replaced by [GitLab][]                    |
| Globe         |                                      | `https://globe.torproject.org`         |                       | Replaced by [Atlas][]                     |
| Help.tpo      | TPA docs and support helpdesk        | `https://help.torproject.org`          | tpa                   | Replaced by this GitLab wiki              |
| [jenkins][]   | continuous integration, autobuilding | `https://jenkins.torproject.org`       | weasel                | Replaced with GitLab CI                   |
| [kvm][]       | virtual machine hosting              | N/A                                    | weasel                | Replaced by [Ganeti][]                    |
| oniongit      | test GitLab instance                 | `https://oniongit.eu`                  | hiro                  | Eventually migrated to GitLab             |
| pipeline      | ?                                    | `https://pipeline.torproject.org`      | ?                     |                                           |
| [Prodromus][] | Web chat for support team            | `https://support.torproject.org`       | phoul, lunar, helix   | ?                                         |
| [Trac][]      | Issues, wiki                         | `https://trac.torproject.org`          | hiro                  | Migrated to GitLab, archived              |
| translation   | Transfifex bridge                    | `majus.torproject.org`                 | emmapeel              | Replaced with [Weblate][]                 |
| XMPP          | Chat/messaging                       |                                        | dgoulet               | Abandoned for lack of users               |

[alternatives]: https://arkenfox.github.io/TZP/
[better]: https://coveryourtracks.eff.org/
[cache]: howto/cache
[jenkins]: service/jenkins
[Trac]: howto/trac
[Prodromus]: https://github.com/rkallensee/prodromus
[Atlas]: https://atlas.torproject.org
[Weblate]: https://hosted.weblate.org/projects/tor/

## Documentation assessment

 * Internal: 20 services, 42% complete
 * External: 20 services, 14 documented, of which 38% are complete
   complete, 6 unknown
 * Undocumented: 23 services
 * Total: 20% of the documentation completed as of 2020-09-30
