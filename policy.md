# Policies

The policies below document major architectural decisions taken in the
history of the team. This process is similar to the [Network Team Meta
Policy][]. More details of the process is available in the first
policy, [tpa-rfc-1-policy](policy/tpa-rfc-1-policy).

[Network Team Meta Policy]: https://gitlab.torproject.org/legacy/trac/-/wikis/org/teams/NetworkTeam/MetaPolicy

To add a new policy, create the page using the [template](policy/template) and add
it to the above list. See the [Writing a TPA-RFC](service/documentation#writing-a-tpa-rfc) section if you're
wondering how to write a policy document or if you should.

## Draft

 * [TPA-RFC-3: tools](policy/tpa-rfc-3-tools)
 * [TPA-RFC-11: SVN retirement](policy/tpa-rfc-11-svn-retirement)
 * [TPA-RFC-17: Disaster recover](policy/tpa-rfc-17-disaster-recovery)
 * [TPA-RFC-18: Security policy](policy/tpa-rfc-18-security-policy)
 * [TPA-RFC-37: Lektor replacement](policy/tpa-rfc-37-lektor-replacement)
 * [TPA-RFC-38: Setting Up a Wiki Service](policy/tpa-rfc-38-new-wiki-service)
 * [TPA-RFC-45: Mail architecture](policy/tpa-rfc-45-mail-architecture)
 * [TPA-RFC-47: Email account retirement](policy/tpa-rfc-47-email-account-retirement)

## Proposed

<!-- No policy is currently `proposed`. -->

 * [TPA-RFC-33: Monitoring](policy/tpa-rfc-33-monitoring)

## Standard

 * [TPA-RFC-1: RFC process](policy/tpa-rfc-1-policy)
 * [TPA-RFC-2: Support](policy/tpa-rfc-2-support)
 * [TPA-RFC-5: GitLab migration](policy/tpa-rfc-5-gitlab)
 * [TPA-RFC-6: Naming Convention](policy/tpa-rfc-6-naming-convention)
 * [TPA-RFC-7: root access](policy/tpa-rfc-7-root)
 * [TPA-RFC-8: GitLab CI libvirt exception](policy/tpa-rfc-8-gitlab-ci-libvirt)
 * [TPA-RFC-14: GitLab artifacts expiry](policy/tpa-rfc-14-gitlab-artifacts)
 * [TPA-RFC-19: GitLab labels](policy/tpa-rfc-19-gitlab-labels)
 * [TPA-RFC-20: bullseye upgrade schedule](policy/tpa-rfc-20-bullseye-upgrades)
 * [TPA-RFC-22: rename TPA IRC channel and Matrix bridge](policy/tpa-rfc-22-rename-irc)
 * [TPA-RFC-24: Extend merge permissions for web projects](policy/tpa-rfc-24-extend-merge-permissions-web)
 * [TPA-RFC-26: LimeSurvey upgrade](policy/tpa-rfc-26-limesurvey-upgrade)
 * [TPA-RFC-27: Python 2 end of life](policy/tpa-rfc-27-python2-eol)
 * [TPA-RFC-30: Changing how lego plugins are used](policy/tpa-rfc-30-change-lego-plugins)
 * [TPA-RFC-32: Nextcloud root-level shared folders migration](policy/tpa-rfc-32-nextcloud-root-level-folders-migration)
 * [TPA-RFC-36: Gitolite, GitWeb retirement](policy/tpa-rfc-36-gitolite-gitweb-retirement)
 * [TPA-RFC-39: Nextcloud account policy](policy/tpa-rfc-39-nextcloud-account-policy)
 * [TPA-RFC-44: Email emergency recovery](policy/tpa-rfc-44-email-emergency-recovery)
 * [TPA-RFC-46: GitLab 2FA](policy/tpa-rfc-46-gitlab-2fa)
 * [TPA-RFC-48: Enable new GitLab Web IDE](policy/tpa-rfc-48-enable-new-gitlab-web-ide)
 * [TPA-RFC-50: private GitLab pages](policy/tpa-rfc-50-private-gitlab-pages)
 * [TPA-RFC-51: Improve l10n review ci workflow](policy/tpa-rfc-51-improve-l10n-review-ci-workflow)
 * [TPA-RFC-55: Swap file policy](policy/tpa-rfc-55-swap-file-policy)
 * [TPA-RFC-56: large file storage](policy/tpa-rfc-56-large-file-storage)
 * [TPA-RFC-57: Debian bookworm upgrade schedule](policy/tpa-rfc-57-bookworm-upgrades)
 * [TPA-RFC-58: Podman CI runner deployment, help needed](policy/tpa-rfc-58-podman-runner)
 * [TPA-RFC-59: ssh jump host aliases](policy/tpa-rfc-59-ssh-jump-host-aliases)
 * [TPA-RFC-60: GitLab 2-factor authentication enforcement](policy/tpa-rfc-60-gitlab-2fa-enforcement)
 * [TPA-RFC-61: 2024 roadmap](policy/tpa-rfc-61-roadmap-2024)
 * [TPA-RFC-62: TPA password manager](policy/tpa-rfc-62-tpa-password-manager)
 * [TPA-RFC-63: Storage server budget](policy/tpa-rfc-63-storage-server-budget)
 * [TPA-RFC-64: Puppet TLS certificates](policy/tpa-rfc-64-puppet-tls-certificates)
 * [TPA-RFC-65: PostgreSQL backups](policy/tpa-rfc-65-postgresql-backups)

## Rejected

 * [TPA-RFC-15: Email services](policy/tpa-rfc-15-email-services) (replaced with TPA-RFC-31)
 * [TPA-RFC-16: Replacing lektor-i18n-plugin](policy/tpa-rfc-16-replacing-lektor-i18n-plugin) (put on hold while we
   consider the Lektor replacement in [TPA-RFC-37](policy/tpa-rfc-37-lektor-replacement))
 * [TPA-RFC-25: BTCpay replacement](policy/tpa-rfc-25-btcpay-replacement)
 * [TPA-RFC-29: Lektor SCSS Plugin](policy/tpa-rfc-29-lektor-scss-plugin)
 * [TPA-RFC-31: outsource email services](policy/tpa-rfc-31-outsource-email) (in favor of TPA-RFC-44
   and following)
 * [TPA-RFC-41: Schleuder retirement](policy/tpa-rfc-41-schleuder-retirement) (kept for the community council)

## Obsolete

 * [TPA-RFC-4: Prometheus disk space change](policy/tpa-rfc-4-prometheus-disk) (one-time change)
 * [TPA-RFC-9: "proposed" status and small process changes](policy/tpa-rfc-9-proposed-process) (merged
   in TPA-RFC-1)
 * [TPA-RFC-10: Jenkins retirement](policy/tpa-rfc-10-jenkins-retirement) (one-time change)
 * [TPA-RFC-12: triage and office hours](policy/tpa-rfc-12-triage-and-office-hours) (merged in TPA-RFC-2)
 * [TPA-RFC-13: Use OKRs for the 2022 roadmap](policy/tpa-rfc-13-okrs-for-roadmap) (2022 past, OKRs not
   used in 2023)
 * [TPA-RFC-21: uninstall SVN](policy/tpa-rfc-21-uninstall-svn) (one-time change)
 * [TPA-RFC-23: retire ipv6only.torproject.net](policy/tpa-rfc-23-retire-ipv6only) (one-time change)
 * [TPA-RFC-28: Alphabetical triage star of the week](policy/tpa-rfc-28-alphabetical-triage) (merged in TPA-RFC-2)
 * [TPA-RFC-34: End of office hours](policy/tpa-rfc-34-office-hours-ends) (merged in TPA-RFC-2)
 * [TPA-RFC-35: GitLab email address changes](policy/tpa-rfc-35-gitlab-email-address-changes) (one-time change)
 * [TPA-RFC-40: Cymru migration budget](policy/tpa-rfc-40-cymru-migration) (replaced by TPA-RFC-43)
 * [TPA-RFC-42: 2023 roadmap](policy/tpa-rfc-42-roadmap-2023)
 * [TPA-RFC-43: Cymru migration plan](policy/tpa-rfc-43-cymru-migration-plan)
 * [TPA-RFC-49: document the ganeti naming convention](https://gitlab.torproject.org/tpo/tpa/wiki-replica/-/merge_requests/39)
 * [TPA-RFC-52: Cymru migration timeline](policy/tpa-rfc-52-cymru-migration-timeline)
 * [TPA-RFC-53: Security keys give away](policy/tpa-rfc-53-security-keys)
 * [TPA-RFC-54: build boxes retirement](policy/tpa-rfc-54-build-boxes-retirement)
