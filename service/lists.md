[[_TOC_]]

Our mailing list server, lists.torproject.org, is running an instance
of mailman.

The "listmaster" team is responsible for configuring all lists as
required.  They make decisions about which lists to create and which
to retire, who should have owner or moderator access to existing
lists, if lists are private, restricted, or public, and many other
aspects of running mailing lists.

If you want to request a new list or propose a change to existing
lists please [file a ticket][].  If "listmaster" approves, they will
coordinate with the admin team to have the list added and then
configure it as needed. Don't forget to update the [list of mailing
lists](#list-of-mailing-lists) (below) upon changes.

# How-to

## create a list

A list can be created by running `newlist` on the mailing list server
(currently `eugeni`):

```
ssh eugeni.torproject.org sudo -u list newlist
```

If you do not have root access, proceed with the mailman admin
password on the [list creation form](https://lists.torproject.org/cgi-bin/mailman/create), which might or might not
work.

Mailman creates the list name with an upper case letter. Usually
people like all lower-case more. So log in to the newly created list
at <https://lists.torproject.org/> and change the list name and the
subject line to lower case.

If people want to have specific settings (no archive, no public
listing, etc.), can you set them also at this stage.

TPA should store the new list password in their password manager.

Don't forget to update the [list of mailing lists](#list-of-mailing-lists) (below) upon
changes and close the trac ticket.

## Remove a list

To remove a list, use the `rmlist` command on `eugeni`:

```
ssh eugeni.torproject.org sudo -u list rmlist LISTNAME
```

By default, archives are kept. If that's not wanted, use the `-a`
flag.

Don't forget to update the [list of mailing lists](#list-of-mailing-lists) upon changes.

## What are our most important lists?

New to Tor? If so then welcome! Our most important lists are as
follows...

 * [tor-dev@](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-dev) - Discussion list for developers, researchers, and
   other technical discussions.
 * [tor-relays@](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-relays) - Discussion list for relay operators.
 * [tor-project@](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-project) - Discussion list for tor contributors. Only
   active and past tor contributors can post to this list.

For general discussion and user questions, [tor-talk@][] was used in the past,
but it has been retired and replaced by the [Tor Project users forum][].

[tor-talk@]: https://lists.torproject.org/pipermail/tor-talk/
[Tor Project users forum]: https://forum.torproject.org

## How do I get permission to post to tor-project@

Just ask. Anyone is allowed to watch, but posting is restricted to
those that actively want to make Tor better. As long as you're willing
to keep your posts constructive just [contact Damian](https://www.atagar.com/contact/).

Note that unlike many of our lists this one is pretty actively
moderated, so unconstructive comments may lose you posting
permissions. Sorry about that, but this is one list we're striving to
keep the noise down on. ;)

## How do I ask for a new mailing list?

Creating a new list is easy, but please only request one if you have a
good reason. Unused lists will periodically be removed to cut down on
bloat. With that out of the way, to request a new list simply [file a
ticket][] with the following...

[file a ticket]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/new

 * What is the list name?
 * What is the email address of the list maintainer? This person will
   be given the list's Mailman administrator password, be notified of
   bounces, and emails to the list owner. If this is a closed list
   then they'll be responsible for maintaining the membership.
 * What is a one sentence description of the list? (see
   [lists.torproject.org](https://lists.torproject.org/cgi-bin/mailman/listinfo) for examples)

Lists default to being public and archived. If you would prefer
something else then you'll need to change its configuration in
Mailman.

Creating lists involves at least two people, so please be patient
while your list is being created.  Be sure to regularly check the
ticket you created for questions by list admins.

Members of tor-internal@ do not require approval for their
lists. Non-members will need sign-off of Damian or qbi.

## Why do we have internal lists?

In additional to our [public email lists](https://www.torproject.org/docs/documentation.html.en#MailingLists) Tor maintains a handful
of communication channels reserved for core contributors. This is not
a secret inner cabal, but rather community members (both paid and
unpaid) who have been long-time contributors with the project. (See
our [Core Contributor Guidelines](https://gitlab.torproject.org/tpo/community/policies/-/blob/HEAD/docs/membership.md).)

Why do we have these internal discussions? Funding proposals, trip
reports, and other things sometimes include details that shouldn't be
public. In general though we strongly encourage discussions to happen
in public instead.

Note that this is a living document. Policies are not set in stone,
and might change if we find something better.

## How do I get added to internal lists?

Internal communication channels are open only to core
contributors. For information on becoming a core contributor, see the
[Core Contributor Guidelines](https://gitlab.torproject.org/tpo/community/policies/-/blob/HEAD/docs/membership.md).

# Reference

## List of mailing lists

### Discussion Lists

The following are lists with subscriber generated threads.

| **List**                     | **Maintainer**                              | **Type** | **Description**                                                                                              |
|------------------------------|---------------------------------------------|----------|--------------------------------------------------------------------------------------------------------------|
| [tor-project](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-project)             | arma, atagar, gamambel                      | Public   | Moderated discussion list for active contributors.                                                           |
| [tor-dev](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-dev)                 | teor, pili, phw, sysrqb, gaba               | Public   | Development related discussion list.                                                                         |
| [tor-onions](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-onions)              | teor, dgoulet, asn, pili, phw, sysrqb, gaba | Public   | technical discussion about running Tor onion (hidden) services                                               |
| [tor-relays](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-relays)              | teor, pili, phw, sysrqb, gaba               | Public   | Relay operation support.                                                                                     |
| [tor-relays-universities](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-relays-universities) | arma, qbi, nickm                            | Public   | Relay operation related to universities (lightly used).                                                      |
| [tor-mirrors](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-mirrors)             | arma, qbi, nickm                            | Public   | Tor website mirror support.                                                                                  |
| [tor-teachers](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-teachers)            | mrphs                                       | Public   | Discussion, curriculum sharing, and strategizing for people who teach Tor around the world.                  |
| [ux](https://lists.torproject.org/cgi-bin/mailman/listinfo/ux)                      | mrphs, isabela, antonela                    | Public   | UX mailing list for discussion about user-experience of Tor products.                                        |
| tor-team                     | arma, atagar, qbi, nickm                    | Private  | Internal discussion list (externally reachable).                                                             |
| tor-internal                 | arma, atagar, qbi, nickm                    | Private  | Internal discussion list.                                                                                    |
| onion-advisors               | isabela                                     | Private  |                                                                                                              |
| onionspace-berlin            | infinity0, juris, moritz                    | Private  | Discussion list for Onionspace, a hackerspace/office for Tor-affiliated and privacy tools hackers in Berlin. |
| onionspace-seattle           | Jon                                         | Private  | Discussion list for the Tor-affiliated and privacy tools hackers in Seattle                                  |
| [global-south](https://lists.torproject.org/cgi-bin/mailman/listinfo/global-south)            | sukhbir, arma, qbi, nickm, gus              | Public   | Tor in the Global South                                                                                      |

### Notification Lists

The following lists are generally read-only for their subscribers. Traffic is either notifications on specific topics or auto-generated.

| **List**                    | **Maintainer**           | **Type** | **Description**                                             |
|-----------------------------|--------------------------|----------|-------------------------------------------------------------|
| [anti-censorship-alerts](https://lists.torproject.org/cgi-bin/mailman/listinfo/anti-censorship-alerts) | phw, cohosh              | Public   | Notification list for anti-censorship service alerts.       |
| [metrics-alerts](https://lists.torproject.org/cgi-bin/mailman/listinfo/metrics-alerts)         |  irl             | Public   | Notification list for Tor Metrics service-related alerts    |
| [regional-nyc](https://lists.torproject.org/cgi-bin/mailman/listinfo/regional-nyc)           | sysrqb                   | Public   | NYC-area Announcement List                                  |
| [tor-announce](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-announce)           | nickm, weasel            | Public   | Announcement of new Tor releases. Here is an [RSS feed](http://rss.gmane.org/gmane.network.tor.announce). |
| [tbb-bugs](https://lists.torproject.org/cgi-bin/mailman/listinfo/tbb-bugs)               | boklm, sysrqb, brade     | Public   | Tor Browser Bundle related bugs.                            |
| [tbb-commits](https://lists.torproject.org/cgi-bin/mailman/listinfo/tbb-commits)            | boklm, sysrqb, brade     | Public   | Tor Browser Bundle related commits to Tor repositories.     |
| [tor-bugs](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-bugs)               | arma, atagar, qbi, nickm | Public   | Tor bug tracker.                                            |
| [tor-commits](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-commits)            | nickm, weasel            | Public   | Commits to Tor repositories.                                |
| [tor-network-alerts](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-network-alerts)     | dgoulet                  | Private  | auto: Alerts related to bad relays detection.               |
| [tor-wiki-changes](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-wiki-changes)       | nickm, weasel            | Public   | Changes to the Trac wiki.                                   |
| [tor-consensus-health](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-consensus-health)   | arma, atagar, qbi, nickm | Public   | Alarms for the present status of the Tor network.           |
| [tor-censorship-events](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-censorship-events)  | arma, qbi, nickm         | Public   | Alarms for if the number of users from a local disappear.   |
| [ooni-bugs](https://lists.torproject.org/cgi-bin/mailman/listinfo/ooni-bugs)              | andz, art                | Public   | OONI related bugs status mails                              |
| tor-svninternal             | arma, qbi, nickm         | Private  | Commits to the internal SVN.                                |

### Administrative Lists

The following are private lists with a narrowly defined purpose. Most have a very small membership.

| **List**                 | **Maintainer**         | **Type** | **Description**                                                                                                                                                                                                                                                  |
|--------------------------|------------------------|----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| tor-security             | dgoulet                | Private  | For reporting security issues in Tor projects or infrastructure. To get the gpg key for the list, contact tor-security-sendkey@lists.torproject.org or get it from pool.sks-keyservers.net. Key fingerprint = 8B90 4624 C5A2 8654 E453  9BC2 E135 A8B4 1A7B F184 |
| [bad-relays](https://lists.torproject.org/cgi-bin/mailman/listinfo/bad-relays)          | dgoulet                | Private  | Discussions about malicious and misconfigured Tor relays.                                                                                                                                                                                                        |
| [board-executive](https://lists.torproject.org/cgi-bin/mailman/listinfo/board-executive)          | isabela                | Private  | |
| [board-finance](https://lists.torproject.org/cgi-bin/mailman/listinfo/board-finance)          | isabela                | Private  | |
| [board-legal](https://lists.torproject.org/cgi-bin/mailman/listinfo/board-legal)          | isabela                | Private  | |
| [board-marketing](https://lists.torproject.org/cgi-bin/mailman/listinfo/board-marketing)          | isabela                | Private  | |
| [meeting-planners](https://lists.torproject.org/cgi-bin/mailman/listinfo/meeting-planners)    | jon, alison            | Public   | The list for planning the bi-annual Tor Meeting                                                                                                                                                                                                                  |
| [membership-advisors](https://lists.torproject.org/cgi-bin/mailman/listinfo/membership-advisors) | atagar                 | Private  | Council advisors on list membership.                                                                                                                                                                                                                             |
| tor-access               | mikeperry              | Private  | Discussion about improving the ability of Tor users to access Cloudflare and other CDN content/sites                                                                                                                                                             |
| tor-employees            | erin                   | Private  | Tor employees                                                                                                                                                                                                                                                    |
| tor-alums                | erin                   | Private  | To support former employees, contractors, and interns in sharing job opportunities                                                                                                                                                                               |
| tor-board                | julius                 | Private  | Tor project board of directors                                                                                                                                                                                                                                   |
| tor-boardmembers-only    | julius                 | Private  | Discussions amongst strictly members of the board of directors, not including officers (Executive Director, President, Vice President and possibly more).                                                                                                        |
| [tor-community-team](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-community-team)  | alison                 | Public   | Community team list                                                                                                                                                                                                                                              |
| [tor-packagers](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-packagers)       | atagar                 | Public   | Platform specific package maintainers (debs, rpms, etc).                                                                                                                                                                                                         |
| [tor-research-safety](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-research-safety) | arma                   | Private  | Discussion list for the Tor research safety board                                                                                                                                                                                                                |
| [tor-scaling](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-scaling)         | arma, nickm, qbi, gaba | Private  | Internal discussion list for performance metrics, roadmap on scaling and funding proposals.                                                                                                                                                                      |
| [tor-test-network](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-test-network)    | dgoulet                | Private  | Discussion regarding the Tor test network                                                                                                                                                                                                                        |
| [translation-admin](https://lists.torproject.org/cgi-bin/mailman/listinfo/translation-admin)   | sysrqb                 | Private  | Translations administration group list                                                                                                                                                                                                                           |
| [wtf](https://lists.torproject.org/cgi-bin/mailman/listinfo/wtf)                 | nickm, sysrqb, qbi     | Private  | a wise tech forum for warm tech fuzzies                                                                                                                                                                                                                          |

### Team Lists

Lists related to subteams within Tor.

| **List**                  | **Maintainer**             | **Type** | **Description**                           |
|---------------------------|----------------------------|----------|-------------------------------------------|
| [anti-censorship-team](https://lists.torproject.org/cgi-bin/mailman/listinfo/anti-censorship-team) | arma, qbi, nickm, phw      | Public   | Anti-censorship team discussion list.     |
| [dir-auth](https://lists.torproject.org/cgi-bin/mailman/listinfo/dir-auth)             | arma, atagar, qbi, nickm   | Private  | Directory authority operators.            |
| [dei](https://lists.torproject.org/cgi-bin/mailman/listinfo/dei)             | TPA   | Public  | Diversity, equity, & inclusion committee            |
| [www-team](https://lists.torproject.org/cgi-bin/mailman/listinfo/www-team)             | arma, qbi, nickm           | Public   | Website development.                      |
| [tbb-dev](https://lists.torproject.org/cgi-bin/mailman/listinfo/tbb-dev)              | boklm, sysrqb, brade       | Public   | Tor Browser development discussion list.  |
| [tor-gsoc](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-gsoc)             | arma, qbi, nickm           | Private  | Google Summer of Code students.           |
| [tor-qa](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-qa)               | boklm, sysrqb, brade       | Public   | QA and testing, primarily for TBB.        |
| [ooni-talk](https://lists.torproject.org/cgi-bin/mailman/listinfo/ooni-talk)            | hellais                    | Public   | Ooni-probe general discussion list.       |
| [ooni-dev](https://lists.torproject.org/cgi-bin/mailman/listinfo/ooni-dev)             | hellais                    | Public   | Ooni-probe development discussion list.   |
| [ooni-operators](https://lists.torproject.org/cgi-bin/mailman/listinfo/ooni-operators)       | hellais                    | Public   | OONI mailing list for probe operators.    |
| [network-health](https://lists.torproject.org/cgi-bin/mailman/listinfo/network-health)       | arma, dgoulet, gk          | Public   | Tor Network Health Team coordination list |
| [tor-l10n](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-l10n)             | arma, nickm, qbi, emmapeel | Public   | reporting errors on translations          |
| [tor-meeting](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-meeting)          | arma                       | Private  | dev. meetings of the Tor Project.         |
| [tor-operations](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-operations)       | smith                 | Private   |  Operations team coordination list       |
| [tpa-team](https://lists.torproject.org/cgi-bin/mailman/listinfo/tpa-team)             | TPA                        | Private  | TPA team coordination list                |

### Internal Lists

We have three email lists ([tor-team@](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-team), [tor-internal@](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-internal), and
[bad-relays@](https://lists.torproject.org/cgi-bin/mailman/listinfo/bad-relays)), and a private IRC channel on OFTC.

 * [tor-team@](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-team) is an invite-only list that is reachable by the
   outside world. As such it both used for email CCs, and receives
   quite a bit of spam.
 * [tor-internal@](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-internal) is an invite-only list that is not reachable by
   the outside world. Some individuals that are especially adverse to
   spam only subscribe to this one.
 * [bad-relays@](https://lists.torproject.org/cgi-bin/mailman/listinfo/bad-relays) is an invite-only list that is reachable by the
   outside world. It is also used for email CCs.
 * Our internal IRC channel is used for unofficial real time internal
   communication.

### Encrypted Mailing Lists

We have mailing lists handled by [Schleuder](https://schleuder.nadir.org/) that we use within
different teams.

 * tor-security@ is an encrypted list. See its entry under
   "Administrative Lists".
 * tor-community-council@ is used by [Community Council](./org/CommunityCouncil)
   members. Anyone can use it to email the community council.

See [schleuder](service/schleuder) for more information on that service.
