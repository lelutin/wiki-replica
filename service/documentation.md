How we manage documentation inside TPA, but also touch on other wikis
and possibly other documentation systems inside TPO.

Note that there is a different service called [status](service/status)
for the status page at <https://status.torproject.org>.

There's also a guide specifically aimed at aiding people write
user-facing documentation in the [Tor User Documentation Style
Guide](https://gitlab.torproject.org/tpo/team/-/wikis/Tor-User-Documentation-Style-Guide).

> The palest ink is better than the most capricious memory.
>
> -- ancient Chinese proverb

[[_TOC_]]

# Tutorial

<!-- test -->

## Editing the wiki through the web interface

If you have the right privileges (currently: being part of TPA, but we
hope to improve this), you should have an `Edit` button at the
top-right of pages in the wiki here:

<https://gitlab.torproject.org/tpo/tpa/team/-/wikis/>

If not (which is more likely), you need to issue a merge request in
the [wiki replica][]. At this URL:

<https://gitlab.torproject.org/tpo/tpa/wiki-replica>

You will see a list of directories and files that constitute all the
pages of the wiki. You need to browse this to find a file you are
interested in editing. You are most likely to edit a [service
page](https://gitlab.torproject.org/tpo/tpa/wiki-replica/-/tree/master/service), say you want to edit this very page. 

 1. Find the [documentation.md](https://gitlab.torproject.org/tpo/tpa/wiki-replica/-/blob/master/service/documentation.md) file in the [service directory](https://gitlab.torproject.org/tpo/tpa/wiki-replica/-/tree/master/service)
    and click it

 2. You should see a "Open in Web IDE" button. If you want the full
    GitLab experience, click that button and good luck. Otherwise,
    click the arrow to the right and select "Edit" than click the
    "Edit" button.

 3. You should now see a text editor with the file content. Make a
    change, say add:
    
        <!-- test -->

    At the top of the file.

 4. Enter a "Commit message" in the field below. Explain why you are
    making the change.

 5. Leave the "Target Branch" unchanged
 
 6. Click "Commit changes". This will send you to a "New merge
    request" page.

 7. Review and expand the merge request description, which is based on
    the previously filled commit message (optional)

 8. Leave all the check boxes as is.

 9. Click "Create merge request".

 10. The wiki administrators will review your request and approve,
     reject, or request changes on it shortly. Once approved, your
     changes should be visible in the wiki.
 
[wiki replica]: https://gitlab.torproject.org/tpo/tpa/wiki-replica

# How-to

<!-- more in-depth procedure that may require interpretation -->

## Editing the wiki through Git

It is preferable to edit the wiki through the [wiki replica][]. This
ensures both the replica and the wiki are in sync, as the replica is
configured to mirror its changes to the wiki. (See the [GitLab
documentation for how this was setup](howto/gitlab#accepting-merge-requests-on-wikis).)

To make changes there, just clone and push to this git repository:

    git clone git@gitlab.torproject.org:tpo/tpa/wiki-replica.git

Make changes, and push. Note that a GitLab CI pipeline will check your
changes and might warn you if you work on a file with syntax
problems. Feel free to ignore those warnings that were already
present, but do be careful at not adding new ones.

Ideally, you should also setup linting locally, see below.

## Local linting configuration

While the wiki replica has continuous integration checks, it might be
good to run those locally, to make sure you don't add any new warnings
when making changes.

We currently lint Markdown syntax (with [markdownlint][]) and spell
check with [codespell][].

### Markdown linting

You can install [markdownlint][] using the upstream instructions, or
run it under docker with a following wrapper:

[markdownlint]: https://github.com/markdownlint/markdownlint

    #!/bin/sh

    exec docker run --volume "$PWD:/data/" --rm -i markdownlint/markdownlint "$@"

Drop this somewhere in your path as `mdl` and it will behave just as
if it was installed locally.

Otherwise markdown lint ships with Debian 13 (trixie) and later.

Then you should drop this in `.git/hooks/pre-commit` (if you want to
enforce checks):

```
#!/bin/bash

${GIT_DIR:-.git}/../bin/mdl-wrapper $(git diff --cached --name-only HEAD)
```

... or `.git/hooks/post-commit` (if you just want warnings):

```
#!/bin/sh

${GIT_DIR:-.git}/../bin/mdl-wrapper $(git diff-tree --no-commit-id --name-only -r HEAD)
```

If you have a document you cannot commit because it has too many
errors, you may be able to convert the whole file at once with a
formatter, including:

 * [prettier](https://prettier.io/) - multi-format, node/javascript, not in Debian
 * [mdformat](https://github.com/executablebooks/mdformat/) - markdown-only, Python, very opiniated, [soon in
   Debian](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1042062)
 * [pandoc](https://pandoc.org/) - multi-format document converter, Haskell, widely
   packaged

Pandoc, in particular, is especially powerful as it has many flags to
control output. This might work for most purposes, including turning
all inline links to references:

    pandoc --from markdown --to commonmark+smart \
      --reference-links --reference-location=section \
      foo.md | sponge foo.md

### Spell checking

The [codespell][] program checks for spelling mistakes in CI. If you
have a CI failure and you just want to get rid of it, try:

    apt install codespell

And then:

    codespell --interactive 3 --write-changes $affected_file.md

Or just:

    codespell -i 3 -w

... to check the entire wiki. There should be no errors in the wiki at
the time of writing.

This should yield very few false positives, but it sometimes *does*
fire needlessly. To skip a line, enter the full line in the
`.codespellexclude` file at the top of the git repository
(`exclude-file = PATH` in the `.codespellrc`).

Some file patterns are skipped in the `.codespellrc` (currently
`*.json`, `*.csv`, and the entire `.git` directory).

You can also add this to a `.git/hooks/pre-commit` shell script:

    codespell $(git diff --cached --name-only --diff-filter=ACM)

This will warn you before creating commits that fail the codespell check.

## Accepting merge requests on wikis

It's possible to work around the [limitation of Wiki permissions](https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/76)
by creating a mirror of the git wiki backing the wikis. This way
more users can suggest changes to the wiki by submitting merge
requests. It's not as easy as editing the wiki, but at least provides
a way for outside contributors to participate.

To do this, you'll need to create [project access tokens](https://gitlab.torproject.org/help/api/README.md#personalproject-access-tokens) in the
Wiki and use the [repository mirror feature](https://gitlab.torproject.org/help/user/project/repository/repository_mirroring.md) to replicate the wiki
into a separate project.

 1. in the Wiki project, head for the **Settings: Access Tokens** page
    and create a new Developer token with `write_repository` access

 2. optionally, create a new project for the wiki, for example called
    [`wiki-replica`](https://gitlab.torproject.org/tpo/tpa/wiki-replica/). you can also use the same project as the wiki
    if you do not plan to host other source code specific to that
    project there. we'll call this the "wiki replica" in either case

 3. in the wiki replica, head for the **Settings / Repository / Mirroring
    repositories** section and fill in the details for the wiki HTTPS
    clone URL:

     * Git repository URL: the HTTPS URL of the Git repository (which
       you can find in the **Clone repository** page on the top-right
       of the wiki) **Important**: Make sure you add a *username* to
       the HTTPS URL, otherwise mirroring will fail. For example, this
       wiki URL:
         
            https://gitlab.torproject.org/tpo/tpa/team.wiki.git
        
         should actually be:
         
            https://wiki-replica@gitlab.torproject.org/tpo/tpa/team.wiki.git

     * **Mirror direction**: `push` (only "free" option, `pull` is
       non-free)
     * **Authentication method**: `Username and Password` (default)
     * **Username**: the **Access token** name created in the first
       step
     * **Password**: the **Access token** secret created in the first
       step
     * **Keep divergent refs**: checked (optional, should make sure
       sync works in some edge cases)
     * **Mirror only protected branches**: checked (to keep merge
       requests from being needlessly mirrored to the wiki)

When you click the **Mirror repository** button, a sync will be
triggered. Refresh the page to see status, you should see the **Last
successful update** column updated. When you push to the replica, the
wiki should be updated.

Naturally, because of limitations of GitLab, you cannot pull changes
from the wiki to the replica. But considering only a limited set of
users have access to the wiki in the first place, this shouldn't be a
problem as long as everyone pushes to the replica.

Another major caveat is that git repositories and wikis have a
different "home page". In repositories, the `README.*` or `index.*`
files get rendered in any directory (including the frontpage). But in
the wiki, it's the `home.md` page and [it is not possible to change
this](https://gitlab.com/gitlab-org/gitlab/-/issues/19842). It's also [not possible to change the landing page on
repositories either](https://gitlab.com/gitlab-org/gitlab/-/issues/20754), a compromise would be to [preview the wiki
home page correctly in repositories](https://gitlab.com/gitlab-org/gitlab/-/issues/432344).

Note that a GitLab upgrade broke this ([issue 41547](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41547)). This was
fixed by allowing web hooks to talk to the GitLab server directly, in
the Admin area. In Admin -> Settings -> Network -> Outbound requests:

 * check `Allow requests to the local network from webhooks and integrations`
 * check `Allow requests to the local network from system hooks`
 * add `gitlab.torproject.org` to `Local IP addresses and domain names
   that hooks and integrations can access`

## Writing a TPA-RFC

This section documents the [TPA-RFC process](policy) for people that
actually want to use it in a practical way. The excruciating details
of how exactly the process works are defined in [TPA-RFC-1](policy/tpa-rfc-1-policy), this
is a more "hands-on" approach.

### Should I make an RFC?

Yes. When in doubt, just make a proposal. It can be as simple as
taking a number here, writing a line in [policy.md](policy) and making an
issue for discussion.

You can even make a proposal and immediately mark it as standard (or
even obsolete!) to just document a thought process, reasoning behind
an emergency change, or something you just need to do now.

### Really? It seems too complicated

It doesn't have to be. Take for example, [TPA-RFC-64: Puppet TLS
certificates](policy/tpa-rfc-64-puppet-tls-certificates). That was originally a short text file weasel pasted
on IRC. Anarcat took it, transformed it to markdown, added bits from
the template, and voila, we have at least some documentation on the
change.

The key idea is to have a central place where decisions and designs
are kept for future reference. You don't *have* to follow the entire
template, write requirements, personas, or make an issue! All you need
is claim a number in the wiki page.

### So what steps are typically involved?

In general, you write a proposal when you have a sticky problem to
solve, or something that needs funding or some sort of
justification. So the way to approach that problem will vary, but an
exhaustive procedure might look something like this:

 1. **make a plan**; brainstorm on the task list: what do you actually
    want to do?  That's the "proposal" section in the template

 2. **personas**; think about your users. make personas representing each class of
    users, picking fun names, make sure you don't make them all dudes
    named Steve, now is a good time to explain gender varieties, and
    don't forget bots! you might want to look at past proposals for
    existing personas, especially if you're reviewing a project that
    was already targeted by a proposal

 3. **review the plan**; personas will change your proposal! if you're
    honest with yourself, you'll notice some inconsistencies and
    negative impacts to your users. try to change your plan to
    accommodate for that or, failing that, document why you can't in
    the "alternatives" section

 4. **requirements**; personas will inform your requirements!
    sometimes you might *start* with requirements, but it's often
    easier to start from users and what they need instead, and then
    deduce requirements from there.

 5. **costs**: by now you should have a good idea of what you're
    doing. break down the tasks in digestible chunks, both in the plan
    but also in a "Costs" section, following the [Kaplan-Moss
    estimation technique][] (see below)

 6. **timeline**: now you have a real task list, try to slot this into
    weeks and months, starting from a fictional approval date (see the
    proposal deadline, which you can set later or change), keep in
    mind vacations and other projects. hint: a 1 week task will take
    more than one week to actually execute!

 7. **summarize and edit**: at this point, you have a pretty complete
    document. think about who will read this, and take time to review
    your work before sending. think about how this will look in an
    email, possible format things so that links are not inline and
    make sure you have a good TL;DR: summary on top. maybe add a
    "Background" section so people know what this is about and a
    "Affected users" to see if they need to read it at all.

 8. **send document for approval**: this will vary wildly depending on
    the affected users, but now you should send your document and hope
    for the best. make it clear it's a proposal and that you welcome
    changes, ideas, or dissent. give plenty of time for discussion and
    grant extensions, if requested and possible. make it clear who
    makes the call ("Approval" section) however. don't forget to mark
    the proposal as such ("Proposed" status) and mark a date in your
    calendar for when you should mark it as adopted or rejected

 9. **reject or adopt**! this is it! either people liked it or not,
    but now you need to either mark the proposal as rejected (and
    likely start thinking about another plan to fix your problem) or
    as "standard" and start doing the actual work, which might require
    creating GitLab issues or, for more complex projects, one or
    multiple milestones and a billion projects.

[Kaplan-Moss estimation technique]: https://jacobian.org/2021/may/25/my-estimation-technique/

### Estimation technique

As a reminder, we first estimate each task's complexity:

| Complexity  | Time              |
|-------------|-------------------|
| small       | 1 day             |
| medium      | 3 days            |
| large       | 1 week (5 days)   |
| extra-large | 2 weeks (10 days) |

... and then multiply that by the uncertainty:

| Uncertainty Level | Multiplier |
| ----------------- | ---------- |
| low               | 1.1        |
| moderate          | 1.5        |
| high              | 2.0        |
| extreme           | 5.0        |

This is hard! If you feel you want to write "extra-large" and
"extreme" everywhere, that's because you haven't broken down your
tasks well enough, break them down again.

See the [Kaplan-Moss estimation technique][] for details.

## Pager playbook

### Wiki unavailable

If the GitLab server is down, the wiki will be unavailable. For that
reason, it is highly preferable to keep a copy of the git repository
backing the wiki on your local computer.

If for some reason you do not have such a copy, it is extremely
unlikely you will be able to read this page in the first place. But,
if for some reason you are able to, you should find the [gitlab](gitlab)
documentation to restore *that* service and then immediately clone a
copy of this repository:

    git@gitlab.torproject.org:tpo/tpa/team.wiki.git

or:

    https://gitlab.torproject.org/tpo/tpa/team.wiki.git

If you can't find the GitLab documentation in the wiki, you can try to
read the [latest copy in the wayback machine](https://web.archive.org/web/2/https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/gitlab/).

## Disaster recovery

If GitLab disappears in a flaming ball of fire, it should be possible
to build a static copy of this website somehow. Originally, GitLab's
wiki was based on [Gollum](https://github.com/gollum/gollum/), a simple Git-based wiki. In practice,
GitLab's design has diverged wildly and is now a [separate
implementation](https://docs.gitlab.com/ee/development/wikis.html).

The GitLab instructions still say you can run `gollum` to start a
server rendering the source git repository to HTML. Unfortunately,
that is done dynamically and cannot be done as a one-time job, or as a
post-update git hook, so you would have to setup [gollum as a
service](https://github.com/gollum/gollum/wiki/Gollum-as-a-service) in the short term.

In the long term, it might be possible to migrate back to [ikiwiki][]
or another static site generator.

[ikiwiki]: https://ikiwiki.info/

# Reference

## Installation

"Installation" was trivial insofar as we consider the [GitLab](gitlab)
step to be abstracted away: just create a wiki inside the `team` and
start editing/pushing content.

In practice, the wiki was migrated from [ikiwiki][] (see [issue
34437](https://gitlab.torproject.org/tpo/tpa/team/-/issues/34437)) using anarcat's [ikiwiki2hugo](https://gitlab.com/anarcat/scripts/raw/master/ikiwiki2hugo.py) converter, which happened
to be somewhat compatible with GitLab's wiki syntax.

The ikiwiki repository was archived inside GitLab in the
[wiki-archive](https://gitlab.torproject.org/tpo/tpa/wiki-archive) and [wiki-infra-archive](https://gitlab.torproject.org/tpo/tpa/wiki-infra-archive) repositories. History of
those repositories is, naturally, also available in the history of the
current wiki.

## SLA

This service should be as available as GitLab or better, assuming TPA
members keep a copy of the documentation cloned on their computers.

## Design

Documentation for TPA is hosted inside a git repository, which is
[hosted inside a GitLab wiki](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/). It is replicated inside a [git
repository at GitLab](https://gitlab.torproject.org/tpo/tpa/wiki-replica) to allow external users to contribute by
issuing pull requests.

GitLab wikis support Markdown, RDoc, AsciiDoc, and Org formats.

### Scope

This documentation mainly concerns the TPA wiki, but there are other
wikis on GitLab which are not directly covered by this documentation
and may have a different policy.

### Structure

The wiki has a *minimalist* structure: we try to avoid deeply nested
pages. Any page inside the wiki should be reachable within 2 or 3
clicks from the main page. Flat is better than tree.

All *services* running at torproject.org MUST have a documentation
page in the [service directory](service) which SHOULD at least include
a "disaster recovery" and "pager playbook" section. It is strongly
encouraged to follow the [documentation template](howto/template) for
new services.

This documentation is based on the [Grand Unified Theory of
Documentation](https://www.divio.com/blog/documentation/), by Daniele Procida. To quote that excellent guide
(which should, obviously, be self-documenting):

> There is a secret that needs to be understood in order to write good
> software documentation: there isn’t one thing called documentation,
> there are four.
> 
> They are: tutorials, how-to guides, technical reference and
> explanation. They represent four different purposes or functions,
> and require four different approaches to their
> creation. Understanding the implications of this will help improve
> most documentation - often immensely.

We express this structure in a rather odd way: each service page has
that structure embedded. This is partly due to limitations in the
tools we use to manage the documentation -- GitLab wikis do not offer
much in terms of structure -- but also because we have a *large*
variety of services being documented. To give a concrete example, it
would not make much sense to have a top-level "Tutorials" section with
tutorials for GitLab, caching, emails, followed by "How to guides"
with guides for... exactly the same list! So instead we flip that
structure around and the top-level structure is by service: within
those pages we follow the suggested structure.

### Style

Writing style in the documentation is currently lose and not formally
documented. But we should probably settle on some english-based,
official, third-party style guide to provide guidance and
resources. The [Vue documentation](https://v3.vuejs.org/guide/) has a [great writing &
grammar section](https://v3.vuejs.org/guide/contributing/writing-guide.html#writing-grammar) which could form a basis here, as well as [Jacob
Kaplan-Moss's Technical Style](https://jacobian.org/2009/nov/11/technical-style/) article.

### Authentication

The entire wiki is public and no private or sensitive information
should be committed to it.

### People

Most of the documentation has been written by anarcat, which may be
considered the editor of the wiki, but any other contributors is
strongly encouraged to contribute to the knowledge accumulating in the
wiki.

### Linting

There is a basic linting check deployed in GitLab CI on the [wiki
replica](https://gitlab.torproject.org/tpo/tpa/wiki-replica), which will run on pull requests and normal
pushes. Naturally, it will not run when someone edits the wiki
directly, as the replica does not pull automatically from the wiki
(because of limitations in the free GitLab mirror implementation).

Those checks are setup in the `.gitlab-ci.yml` file. There is a basic
`test` job that will run whenever a Markdown (`.md`) file gets
modified. There is a rather convoluted pipeline to ensure that it runs
*only* on those files, which requires a separate Docker image and job
to generate that file list, because the [`markdownlint/markdownlint`
Docker image](https://hub.docker.com/r/markdownlint/markdownlint) doesn't ship with git (see [this discussion for
details](https://forum.gitlab.com/t/ci-cd-pipeline-get-list-of-changed-files/26847/16?u=anarcat)).

There's a separate job (`testall`) which runs every time and checks
*all* markdown files.

Because GitLab has this... [unusual syntax](https://docs.gitlab.com/ee/user/markdown.html#table-of-contents) for triggering the
automatic table of contents display (`[[_TOC_]]`), we need to go
through some hoops to silence those warnings. This implies that the
`testall` job will always fail, as long as we use that specific macro.

Those linting checks could eventually be expanded to do more things,
like spell-checking and check for links outside of the current
document. See the [alternatives considered](#alternatives-considered) section for a broader
discussion on the next steps here.

## Issues

There is no issue tracker specifically for this project, [File][] or
[search][] for issues in the [team issue tracker][search] with the
~Documentation label.

 [File]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/new
 [search]: https://gitlab.torproject.org/tpo/tpa/team/-/issues?label_name%5B%5D=Documentation

Notable issues:

 * [wikis are not editable publicly](https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/76)
 * [wikis are hard to find](https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/66)
 * [wiki migration is incomplete](https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/5)

See also the [limitations](#limitations) section below.

## Monitoring and testing

There is not monitoring of this service, outside of the main GitLab
monitoring systems.

There are no continuous tests of the documentation.

See the "alternatives considered" section for ideas on tests that
*could* be ran.

## Logs and metrics

No logs or metrics specific to the wiki are kept, other than what
GitLab already does.

## Backups

Backed up alongside GitLab, and hopefully in git clones on all TPA
members machines.

## Other documentation

 * [GitLab wiki documentation](https://docs.gitlab.com/ee/user/project/wiki/#wiki)
 * [GitLab markdown format](https://docs.gitlab.com/ee/user/markdown.html)

# Discussion

Documentation is a critical part of any project. Without
documentation, things lose their meaning, training is impossible, and
memories are lost. Updating documentation is also hard: things change
after documentation is written and keeping documentation in sync with
reality is a constant challenge.

This section talks about the known problems with the current
documentation (systems) and possible solutions.

## Limitations

### Redundancy

The current TPA documentation system is a GitLab wiki, but used to be
a fairly old [ikiwiki][] site, part of the [static site system](howto/static-component).

As part of the ikiwiki migration, that level of redundancy was lost:
if GitLab goes down, the wiki goes down, along with the
documentation. This is mitigated by the fact that the wiki is backed
by a Git repository. So TPA members are strongly encouraged to keep a
copy of the Git repository locally to not only edit the content (which
makes sure the copy is up to date) but also consult it in case of an
infrastructure failure.

### Unity

We have lots of documentation spaces. There's this wiki for TPA, but
there are also different wikis for different teams. There's a proposal
to create a [community hub](https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/66) which could help. But that idea assumes
people will know about the hub, which adds an extra layer of indirection.

It would be better if we could have group wikis, which were [published
as part of the 13.5 release](https://about.gitlab.com/releases/2020/10/22/gitlab-13-5-released/) but, unfortunately, only in the
commercial version. So we're stuck with our current approach of having
the "team" projects inside each group to hold the wiki.

It should also be noted that we have documentation scattered outside
the wiki as well: some teams have documentation in text files, others
are entire static websites. The above community hub could benefit from
linking to those other resources as well.

### Testing

There is no continuous testing/integration of the documentation. Typos
frequently show up in documentation, and probably tons of broken links
as well. Style is incoherent at best, possibly unreadable at
worst. This is a tough challenge in any documentation system, due to
the complexity and ambiguity of language, but it shouldn't deter us
from running basic tests on the documentation.

This would require hooking up the wiki in GitLab CI, which is not
currently possible within GitLab wikis. We'd need to switch the wiki
to a full Git repository, possibly pushing to the wiki using a deploy
key on successful runs. But then why would we keep the wiki?

### Structure

Wikis are notorious for being hard to structure. They can quickly
become a tangled mess with oral tradition the only memory to find your
way inside of the forest. The GitLab wikis are especially vulnerable
to this as they do not offer many tools to structure content: no
includes, limited macros and so on.

The *is* a mechanism to [add a sidebar](https://docs.gitlab.com/ee/user/project/wiki/#customizing-sidebar) in certain sections, that
said, which can help quite a bit in giving a rough structure. But
restructuring the wiki is hard: renaming pages breaks all links
pointing to it and [there is no way to do redirects](https://gitlab.com/gitlab-org/gitlab/-/issues/257892) which is a
major regression from ikiwiki. Note that we can inject redirections at
the Nginx level, see [tpo/web/team#39](https://gitlab.torproject.org/tpo/web/team/-/issues/39) for an example, but this
requires administrator access.

Using a static site generator (SSG) could help here: many of them
support redirections (and so [does GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/redirects.html), although in a
very limited way). Many SSGs also support more "structure" features
like indexes, hierarchical (and automatic) sidebars (based on
structure, e.g. [Sphinx](https://www.sphinx-doc.org/) or [mkdocs](https://www.mkdocs.org/)), paging, per-section RSS
feeds (for "blog" or "news" type functionality) and so on.

The "Tutorial/Howto/Reference/Discussion" structure is not as intuitive
as one author might like to think. We might be better reframing this
in the context of a service, for example merging the "Discussion" and
"Reference" section, and moving the "Goals/alternatives considered"
section into an (optional?)  "Migration" section, since that is really
what the discussion section is currently used for (planning major
service changes and improvements).

The "Howto" section could be more meaningfully renamed "Guides", but
this might break a lot of URLs.

### Syntax

Markdown is great for jotting down notes, filing issues and so on, but
it has been [heavily criticised](https://www.ericholscher.com/blog/2016/mar/15/dont-use-markdown-for-technical-docs/) for use in formal
documentation. One of the problem with Markdown is its lack of
standardized syntax: there is [CommonMark](http://commonmark.org/) but it has yet to see
wider adoption.

This makes Markdown not portable across different platforms supposedly
supporting markdown.

It also lacks special mechanisms for more elaborate markups like
[admonitions](http://docutils.sourceforge.net/0.7/docs/ref/rst/directives.html#admonitions) (or generally: "semantic meanings") or "quick links"
(say: bug#1234 pointing directly to the bug tracker). (Note that there
*are* special extensions to handle this in Markdown, see
[markdown-callouts](https://github.com/oprypin/markdown-callouts) and the [admonition extension](https://python-markdown.github.io/extensions/admonition/).

It has to be said, however, that Markdown is widely used, much more
than the alternatives (e.g. asciidoc or rst), for better or for
worse. So it might be better to stick with it than to force users to
learn a new markup language, however good it is supposed to be.

### Editing

Since few people are currently contributing to the documentation, few
people review changes done to it. As [Jacob Kaplan-Moss quipped](https://jacobian.org/2009/nov/12/editors/):

> All good writers have a dirty little secret: they’re not really that
> good at writing. Their editors just make it seem that way.

In other words, we'd need a technical writer to review our docs, or at
least setup a self-editing process the way Kaplan-Moss suggests above.

### Templating

The current "service template" has one major flaw: when it is updated,
the editor needs to manually go through *all* services and update
those. It's hard to keep track of which service has the right headings
(and is up to date with the template).

One thing that would be nice would be to have a way to keep the
service pages in sync with the template. I [asked for suggestions in
the Hugo forum](https://discourse.gohugo.io/t/using-and-maintaining-markdown-templates/31020), where a simple suggestion was to version the
template and add that to the instances, so that we can quickly see
when a dependency needs to be updated.

To do a more complete comparison between templates and instances, I
suspect I will have to roll my own, maybe something like [mdsaw](https://gitlab.com/hydrargyrum/mdsaw)
but using a real parse tree.

Note that there's also [emd](https://mh-cbon.github.io/emd/) which is a "Markdown template
processor", which could prove useful here (untested).

See also [scaraplate](https://scaraplate.readthedocs.io/) and [cookiecutter](https://cookiecutter.readthedocs.io/).

## Goals

Note: considering we *just* migrated from ikiwiki to GitLab wikis, it
is unlikely we will make any major change on the documentation system
in the short term, unless one of the above issues becomes so critical
it needs to immediately be fixed.

That said, improvements or replacements to the current system should
include...

### Must have

 * highly available: it should be possible to have readonly access to
   the documentation even in case of a total catastrophe (global
   [EMP](https://en.wikipedia.org/wiki/Electromagnetic_pulse) catastrophe excluded)

 * testing: the documentation should be "testable" for typos, broken
   links and other quality issues

 * structure: it should be possible to structure the documentation in
   a way that makes things easy to find and new users easily orient
   themselves

 * discoverability: our documentation should be easy to find and
   navigate for new users

 * minimal friction: it should be easy to contribute to the
   documentation (e.g. the "Edit" button on a wiki is easier than
   "make a merge request", as a workflow)

### Nice to have

 * offline write: it should be possible to write documentation offline
   and push the changes when back online. a git repository is a good
   example of such functionality

 * nice-looking, easily themable

 * coherence: documentation systems should be easy to cross-reference
   between each other

 * familiarity: users shouldn't have to learn a new markup language or
   tool to work on documentation

### Non-Goals

 * repeat after me: we should not write our own documentation system

## Approvals required

TPA, although it might be worthwhile to synchronize this technology
with other teams so we have coherence across the organisation.

## Proposed Solution

We currently use GitLab wikis.

## Cost

Staff hours, hosting costs shadowed by GitLab.

## Alternatives considered

### Static site generators

 * [11ty](https://www.11ty.dev/): [picked by mozilla](https://hacks.mozilla.org/2020/10/to-eleventy-and-beyond/), javascript
 * [antora](https://antora.org/): used by the Emacs [projectile](https://docs.projectile.mx/projectile/) project, multiple
   project support, asciidoc, javascript
 * [docusaurus](https://docusaurus.io/): React, MDX, made by Meta/Facebook, single-page
   app, used for Arti docs
 * [hugo](https://gohugo.io/): golang, multiple themes, plugins, see for example
   [geekdocs](https://geekdocs.de/) for a docs oriented theme and [docsy](https://www.docsy.dev/) for one
   heavily used around Google projects like [kubernetes](https://kubernetes.io/), [grpc](https://www.grpc.io/)
   or [protobufs](https://protobuf.dev/)
 * [ikiwiki][]: previously used, old, Perl, hard to setup, occult   templating system, slow
 * [lektor](https://www.getlektor.com/): used at Tor for other public websites
 * [mdBook](https://rust-lang.github.io/mdBook/): useful to build user manuals, used by the Rust
   community, [considered by the network team](https://gitlab.torproject.org/tpo/core/torspec/-/blob/98b281544c712d993be14a34b8e01fe676edacf9/proposals/345-specs-in-mdbook.md)
 * [Mkdocs](https://www.mkdocs.org/): also supported by [Read the docs](https://readthedocs.org/), similar to
   Sphinx, but uses Markdown instead of RST, [mike](https://github.com/jimporter/mike) can be used to
   support multiple versions in GitHub Pages
 * [Nanoc][]: [used by GitLab](https://docs.gitlab.com/ee/development/documentation/site_architecture/index.html)
 * [pelican](https://getpelican.com/): watch out for pelican, another user reports that,
   with caching, generating a 500 page site takes 30 seconds, 2
   minutes without caching
 * [Sphinx](https://www.sphinx-doc.org/): used by [Read the docs](https://readthedocs.org/), enforces more structure
   and more formal (if less familiar) markup (ReStructured Text, RST),
   [has plugin for redirections](https://github.com/wpilibsuite/sphinxext-rediraffe), see also [rstfmt](https://github.com/dzhu/rstfmt) a
   linter/formatter for RST
 * [zola](https://www.getzola.org/): rust

[Nanoc]: https://nanoc.ws/

### Tools currently in use

 * Docusaurus: [arti docs](https://gitlab.torproject.org/tpo/core/arti-doc-project-2023)
 * GitLab wikis
 * Hugo: <https://status.torproject.org> <https://research.torproject.org>
 * Lektor: most websites, see [the site list](https://gitlab.torproject.org/tpo/web/team/-/wikis/home)
 * mbBook: <https://spec.torproject.org>
 * mkdocs: [hackweek](https://tpo.pages.torproject.net/community/hackweek/)

### mkdocs

I did a quick test of mkdocs to see if it could render the TPA wiki
without too many changes. The [result](https://paste.anarc.at/publish/2021-10-22-_GMTXBk9IcmLXMlXGGEGjitDFmpf3gZ3R5TjPG738IU/) is not so bad! I am not a
fan of the mkdocs theme, but it does work, and has prev/next links
like a real book which is a nice touch (although maybe not useful for
us, outside of meetings maybe). Navigation is still manual (defined in
the configuration file instead of a sidebar).

Syntax is not entirely compatible, unfortunately. The GitLab wiki has
this unfortunate habit of expecting "semi-absolute" links everywhere,
which means that to link to (say) this page, we do:

    [documentation service](service/documentation)

... from anywhere in the wiki. It seems like mkdocs expects relative
links, so this would be the same from the homepage, but from the
service list it should be:

    [documentation service](documentation)

... and from a sibling page:

    [documentation service](../documentation)

Interestingly, mkdocs warns us about broken links directly, which is a
nice touch. It found this:

```
WARNING -  Documentation file 'howto.md' contains a link to 'old/new-machine.orig' which is not found in the documentation files. 
WARNING -  Documentation file 'old.md' contains a link to 'old/new-machine.orig' which is not found in the documentation files. 
WARNING -  Documentation file 'howto/new-machine.md' contains a link to 'howto/install.drawio' which is not found in the documentation files. 
WARNING -  Documentation file 'howto/rt.md' contains a link to 'howto/org/operations/Infrastructure/rt.torproject.org' which is not found in the documentation files. 
WARNING -  Documentation file 'policy/tpa-rfc-1-policy.md' contains a link to 'policy/workflow.png' which is not found in the documentation files. 
WARNING -  Documentation file 'policy/tpa-rfc-9-proposed-process.md' contains a link to 'policy/workflow.png' which is not found in the documentation files. 
WARNING -  Documentation file 'service/forum.md' contains a link to 'service/team@discourse.org' which is not found in the documentation files. 
WARNING -  Documentation file 'service/lists.md' contains a link to 'service/org/operations/Infrastructure/lists.torproject.org' which is not found in the documentation files. 
```

A full rebuild of the site takes 2.18 seconds. Incremental rebuilds
are not faster, which is somewhat worrisome.

### hugo

Tests with hugo were really inconclusive. We had to do `hugo new site
--force .` for it to create the necessary plumbing to have it run at
all. And then it failed to parse many front matter, particularly in
the policy section, because they are not quite valid YAML blobs
(because of the colons). After fixing *that*, it ran, but completely
failed to find *any* content whatsoever.

### Lektor

Lektor is similarly challenging: all files would need to be re-written
to add a `body:` tag on top and renamed to `.lr`.

### Testing

To use those tests, wikis need to be backed by a GitLab project (see
[Accepting merge requests on wikis](howto/gitlab#accepting-merge-requests-on-wikis)), as it is not (currently)
possible to run CI on changes in GitLab wikis.

 * GitLab has a [test suite](https://docs.gitlab.com/ee/development/documentation/testing.html) for their documentation which:
   * runs the [nodejs markdownlint][]: checks that Markdown syntax
   * runs [vale][]: grammar, style, and word usage linter for the
     English language
   * checks the internal anchors and links using Nanoc
 * [codespell][] checks for typos in program source code, but also
   happens to handle Markdown nicely, it can also apply corrections
   for errors it finds, an alternative is [typos](https://github.com/crate-ci/typos), written in Rust
 * [Danger systems](https://danger.systems/ruby/) has a bunch of plugins which could be used to
   check documentation ([lefthook](https://github.com/evilmartians/lefthook), [precious](https://github.com/houseabsolute/precious), and
   [pre-commit](https://pre-commit.com/) ([in Debian](https://tracker.debian.org/pkg/pre-commit-hooks)) are similar wrappers)
 * [textlint](https://textlint.github.io/): pluggable text linting approach recognizing markdown
 * [proselint](http://proselint.com/): grammar and style checking
 * [languagetool](https://www.languagetool.org/): Grammar, Style and Spell Checker
 * [anorack](https://github.com/jwilk/anorack): spots errors based on phonemes
 * [redpen](https://redpen.cc/): huge JAR, can be noisy
 * [linkchecker](https://github.com/linkchecker/linkchecker/): can check links in HTML (anarcat is one of the
   maintainers), has many alternatives, see for example [lychee](https://github.com/lycheeverse/lychee),
   [muffet](https://github.com/raviqqe/muffet), [hyperlink](https://github.com/untitaker/hyperlink), [more](https://gitlab.torproject.org/tpo/tpa/ci-templates/-/issues/14#note_2843661))
 * [forspell](https://github.com/kkuprikov/forspell): wrapper for hunspell, can deal with (Ruby, C, C++)
   source code, local dictionaries
 * [ls-lint](https://ls-lint.org/): linter for filenames

See also [this LWN article](https://lwn.net/Articles/822969/).

Note that we currently use [markdownlint][], the Ruby version, not the
Node version. This was primarily because anarcat dislikes Node more
than Ruby, but it turns out the Ruby version also has more
features. Notably, it can warn about Kramdown compilation errors, for
example finding broken Markdown links.

We also do basic spell checking with [codespell][] mostly because it
was simple to setup (it's packaged in Debian while, say, vale isn't)
but also because it has this nice advantage of supporting Markdown and
it's able to make changes inline.

[vale]: https://errata-ai.gitbook.io/vale/
[nodejs markdownlint]: https://github.com/DavidAnson/markdownlint
[codespell]: https://github.com/codespell-project/codespell/

#### Vale

Vale is interesting: it's used by both GitLab and Grafana to lint
their documentation. Here are their (extensive) rule sets:

 * [Grafana](https://github.com/grafana/writers-toolkit/tree/main/vale/Grafana)
 * [GitLab](https://gitlab.com/gitlab-org/gitlab/-/tree/master/doc/.vale/gitlab) (only one of the configurations, [their guide has
   more](https://docs.gitlab.com/ee/development/documentation/testing/vale.html))

In a brief test against a couple of pages in TPA's wiki, it finds a
*lot* of spelling issues, mostly false positives (like `GitLab`, or
`Grafana`), so we'd have to build a dictionary to not go bonkers. But
it does find errors that codespell missed. We *could* bootstrap from
[GitLab's dictionary](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/.vale/gitlab/spelling-exceptions.txt), hooked from their [spelling rule](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/.vale/gitlab/Spelling.yml).

#### mlc

[mlc](https://github.com/becheran/mlc) was tested briefly as part of [the check links issue](https://gitlab.torproject.org/tpo/tpa/ci-templates/-/issues/14) and
found to not handle internal GitLab wiki links properly (although that
might be a problem for all link checkers that operate on the source
code). It also [doesn't handle anchors](https://github.com/becheran/mlc/issues/31), so it was discarded.

### Charts and diagrams

We currently use [Graphviz](https://graphviz.org/) to draw charts, but have also used
[Diagrams.net](https://www.diagrams.net/) (formerly `draw.io`). Other alternatives:

 * [Mermaid](https://mermaid-js.github.io/mermaid/#/) - builtin to GitLab and GitHub
 * [Knotend](https://www.scottantipa.com/why-knotend)
 * [Flowchart](https://flowchart.fun/)
 * [Kroki](https://kroki.io/) does basically all of those, and [niolesk](https://niolesk.top) is a web
   app on top of that
 * [C4 model](https://c4model.com/) - kind of a watered-down version of UML, they have a
   [great checklist](https://c4model.com/review/)
 * [typograms](https://google.github.io/typograms/)
 * [gaphor](https://gaphor.org/) - [not in Debian](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1056299)
 * [wirewiz](https://github.com/wireviz/WireViz) - cabling diagrams

TODO: make a section about diagrams, how to make them, why they are
useful, etc. See [this for inspiration](https://sportebois.medium.com/better-architecture-diagrams-for-agile-teams-actionable-tips-and-lessons-e76627dc4315). Also consider [DRAKON
diagrams](https://en.wikipedia.org/wiki/DRAKON?wprov=sfla1), found through this [visual guide on when to shut up](https://shaungallagher.pressbin.com/blog/shut-up.html).

Normal graphic design tools like Inkscape, Dia, Krita and Gimp can of
course be used for this purpose. Ideally, an editable and standard
vector format (e.g. SVG) should be used for future proofing.

For this, clipart and "symbols" can be useful to have reusable
components in graphs. A few sources:

 * [inkscape-open-symbols](https://github.com/PanderMusubi/inkscape-open-symbols) ([Debian package](https://tracker.debian.org/inkscape-open-symbols)): ships a bunch of
   icons from multiple sources (e.g. Font Awesome) directly usable in
   Inkscape, lacking a few which are suggested [IMKL](https://github.com/PanderMusubi/inkscape-open-symbols/issues/91), [OSA](https://github.com/PanderMusubi/inkscape-open-symbols/issues/38),
   [Visio-like](https://github.com/PanderMusubi/inkscape-open-symbols/issues/25)
 * [Free SVG](https://freesvg.org/): SVG vector files CC0
 * [Feather icons](https://feathericons.com/): more icons than symbols or clipart, MIT
 * [SVG repo](https://www.svgrepo.com/)
 * [Lucide](https://lucide.dev/): Icons, ISC license
 * [Icon Buddy](https://iconbuddy.app/): icons, various licenses
 * [Hero icons](https://heroicons.com/): icons
 * [AWS icons](https://awsicons.dev/)
 * [Google icons](https://cloud.google.com/icons)

Note that Inkscape has rudimentary routing with the [connector tool](https://wiki.inkscape.org/wiki/Connector_tool_tutorial).
