# Torproject Sysadmin Team

The Torproject System Administration Team is the team that keeps
torproject.org's infrastructure going. This is the internal team wiki.
It has mostly documentation mainly targeted for the team members, but
may also have useful information for people with torproject.org
accounts.

The documentation is split into the following sections:

 * [Support](support) - in case of fire, press this button
 * [User documentation](doc) - aimed primarily at non-technical users
   and the general public
 * [Sysadmin how-to's](howto) - procedures specifically written for sysadmins
 * [Service list](service) - service list and documentation
 * [Machine list](https://db.torproject.org/machines.cgi) - the full
   list of machines managed by TPA (in LDAP)
 * [Policies](policy) - major decisions and how they are made
 * [Meetings](meeting) - minutes from our formal meetings
 * [Roadmaps](roadmap) - documents our plans for the future (and past
   successes of course)

<!-- when the above section is updated, _sidebar.md must be as well. -->

Our source code is all hosted on [GitLab](https://gitlab.torproject.org/tpo/tpa).

----

This is a wiki. We welcome changes to the content!  If you have the
right permissions -- which is [actually unlikely, unfortunately](https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/76) --
you can edit the wiki in GitLab directly. Otherwise you can submit a
[pull request]() on the [wiki replica](https://gitlab.torproject.org/tpo/tpa/wiki-replica). You can also [clone the git
repository](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/git_access) and [send us a patch by email](support).

To implement a similar merge request workflow on *your* GitLab wiki,
see [TPA's documentation about Accepting merge requests on wikis](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/documentation#accepting-merge-requests-on-wikis).
